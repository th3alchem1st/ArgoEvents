import pika

f = open('./subdomains.txt', 'r')

for subdomain in f:
	sub = subdomain.rstrip()
	print sub

	connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
	channel = connection.channel()
	channel.basic_publish(exchange='test', routing_key='hello', body='{"message": "' + sub + '"}')
